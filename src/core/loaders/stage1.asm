@regset s45
@org 0xFA00

;; Sets a lot of things to zero
mov r1, [&ZEROS]
mov [&ADDRSEL2], r1
mov [&BUSCON2], r1
mov [&ADDRSEL3], r1
mov [&BUSCON3], r1
mov [&ADDRSEL4], r1
mov [&BUSCON4], r1
mov r4, [&ZEROS]
mov ~DPP0, 0x0

lb4:
callr ramtest

;; Sets interrupt vector for Timer 3 
;; (FA 00 00 FC) -> JMPS 0x00, 0xFC00
mov ~DPP1, 0x0 
mov r2, 0x408C
mov r3, 0x00FA 
mov [r2], r3 ; *(0x00008C) = 0x00FA
mov r3, 0xFC00
mov [r2 + 0x2], r3 ; *(0x00008E) = 0xFC00
bset ~PSW.11 ; Enable CPU Interrupts 

; TODO this should probably never trigger
cmp r4, 0x0 
jmpr cc_ne, ramFault

mov [0xFB42], ~ZEROS ; *(0x00FB42) = 0x0000 TODO: purpose

add ~DPP0, 0x01 ; should result in DPP0 = 0x01
cmp ~DPP0, 0x03 ; should never trigger
jmpr cc_ne, lb3
add ~DPP0, 0x09 ; ~> DPP0 = 0x0A
lb3:
  cmp ~DPP0, 0x10
  jmpr cc_ult, lb4 ; Loop to beginning if DPP0 <= 0x10

  ;; *([0xFB44 .. 0xFBBE]) = 0
  mov r0, 0xFBBE 
  lb31:
    mov [r0], [&ZEROS]
    cmpd2 r0, 0xFB42
    jmpr cc_ne, lb31

  ; return to stage 0 code loaded at 0xFC00 
  rets

;; ramtest failed
ramFault:
  add r4, 0x10
  mov r3, r4 ; r3 = Error code + 0x10

  ; tx *([0xFA8F .. 0xFA93]) to Serial0
  ; corresponds to bytes FF FF FF 02 18
  mov r1, 0xFA8F
  mov r4, 0xFA94
  .txLoop:
    movb rl2, [r1+]
    callr txByte
    cmp r1, r4
    jmpr cc_ne, .txLoop

  ; tx 0x10 + error code (see ramtest)
  movb rl2, rl3
  callr txByte
  
  ; tx (0x10 + error code) XOR 0xE5 ; TODO: Find out why this pattern is used
  xorb rl3, 0xE5
  movb rl2, rl3
  callr txByte

  ; halt system
  loopinf:
    jmpr cc_uc, loopinf

; @org 0xFA8E ; should be aligned as is
;; TODO: Why?
db 0x00, 0xFF, 0xFF, 0xFF, 0x02, 0x18

;; Transmits rl2 to Serial0
txByte:
  bclr ~S0TIC.7
  movb [&S0TBUF], rl2
  .waitForTx:
    jnb ~S0TIC.7, .waitForTx
  ret

;; ramtest
; returns r4:
;   0  No Errors
;   1  Error detected while scanning with sliding AND 0xFFFF mask
;   2  Error detected while writing complemented address values
;   3  Error detected while scanning with sliding OR 0x0000 mask
ramtest:
  mov r2, [&ONES]
  mov r1, 0x4000 ; Raw address due to subseq. decrement and DPP0 = 0 (see top)

  ; *([0x0000 .. 0x3FFE]) = 0xFFFF
  .fillAllOnes:
    mov [-r1], r2
    cmp r1, 0x0 
    jmpr cc_ne, .fillAllOnes

  ; ANDS all written locations with r2 (0xFFFF) again
  .testAllLocations:
    and r2, [r1+]
    cmp r1, 0x4000
    jmpr cc_ne, .testAllLocations

  ; If any location was faulty, return 1
  cmp r2, 0xFFFF
  jmpr cc_ne, .rtError1

  ; Tests all memory locations by writing the complement of their
  ; addresses to them
  .testWithInverseAddr:
    mov r2, [&DPP0]
    shl r2, 0xE
    or r2, r1
    cpl r2 ; r2 = ~(DPP0 >> 14 | r1) -> ~(0 | r1) = ~r1

    mov [-r1], r2
    mov r3, [r1 + 0x2] ; Why? r3 is not used afterwards
    cmp r2, [r1]
    jmpr cc_ne, .rtError2
    cmp r1, 0x0 
    jmpr cc_ne, .testWithInverseAddr

  ; Tests all locations by writing zeroes to them
  ; Otherwise very similar to test 1
  mov r2, [&ZEROS]
  mov r1, 0x4000

  .fillAllZeroes:
    mov [-r1], r2
    or r2, [r1]
    cmp r1, 0x0 
    jmpr cc_ne, .fillAllZeroes

  cmp r2, 0x0 
  jmpr cc_ne, .rtError3
  ret

  ;; Faulty location found by OR testing
  .rtError3:
  add r4, 0x1

  ;; Faulty location found by inverse address write test
  .rtError2:
  add r4, 0x1 

  ;; Faulty location found by AND testing
  .rtError1: 
  add r4, 0x1
  ret

