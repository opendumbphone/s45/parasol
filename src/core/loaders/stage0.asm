@regset s45
@define S0TIR ~S0TIC.7
@define S0RIR ~S0RIC.7

;; Loader file pads this here (results in Bad alignment, ig)
db 0x00

@org 0xFA00 ; See bootFromSerial ROM routine and Freia docs

;; Begin of initial BSL
diswdt
mov ~DPP3, 0x03
nop ; Required due to DPP set

mov ~CP, 0xFB00

; BUS 0, Bus 1: No MCTC Waitstates, r/w delay, 16bit demultiplexed, normal ALE, enabled
; (W: Reserved Bit 8 is set)
mov ~BUSCON0, 0x05AF
mov ~BUSCON1, 0x05AF

; TODO: Freia docs say this is 256KB, but page 9-24 says otherwise
; Also this seems to interfere with the register spaces?
mov ~ADDRSEL1, 0x07 ; 512KiB @0x000000 [0x000000 - 0x080000] 

; TODO: Absolutely no idea what this does. Undocumented register
extr 1 
bset 0x9A.15 ; [0xF134]

; SYSCON: XPEN, VISIBLE (XBUS peripherals enabled and visible), ROM enabled and on Segment 1
mov ~SYSCON, 0x1406

; DPP0 = 0, DPP1 = 3, DPP3 = 3
mov r1, 0x0 
mov [&DPP0], r1
mov ~DPP1, 0x03
mov ~DPP3, 0x03

mov r0, [r1] ; r0 = *(0x0)
mov r2, r0 ; r2 = r0
cpl r0 ; r0 ~= r0

; Write inverted read to same address
movb [r1], rl0
movb [r1 + 0x1], rh0

; Write initial read to *(0x2)
mov [r1 + 0x2], r2
cmp r0, [r1] ; Checks whether the previous write has actually worked (?)
mov [&BUSCON1], r1 ; Zeroes out BUSCON1

;; XBUS Config ;;
; TODO: extr 4 once everything works
extr 1 
mov ~XADDRS5, 0x04 ; 64KiB XRAM @0x000000 [0x000000 - 0x010000]
extr 1
mov ~XADDRS6, 0x0304 ; 64KiB XRAM @0x030000 [0x030000 - 0x040000]

; XBUS 5/6: No MCTC Waitstates, r/w delay, 16bit demultiplexed, normal ALE, enabled
; (if these bitfields are truly the same as ADDRSEL and BUSCON)
extr 1 
mov ~XBCON5, 0x04AF 
extr 1
mov ~XBCON6, 0x04AF

;; Copies the code below to 0xFC00, then executes it starting at entryNextStage
;; TODO: Improve the assembler to make these moves and jumps label-dependent
mov r3, 0x7C00 ; Note: DPP2 selected. DPP2 = 3 -> addr is 0xFC00
mov r13, 0xFA98
mov r14, 0x7C5C
callr memcpy ; copy [0xFA98 .. 0xFAF2] -> [0xFC00 .. 0xFC5A] (boundaries inclusive; 90 bytes)

mov [0xFB40], r14 ; *(0xFB40) = 0x7C5C ; TODO: Unknown loc
; TODO: Freia says this is watchdog related?
bset ~P4.1
bset ~DP4.1

; Timer 3: 3170 Ticks, Timer mode, f = 32.23 kHz, running, count down
; Underflow after ~0.1s
; Interrupt: GLVL 0, ILVL 4, Enabled
mov ~T3, 0x0C62
mov ~T3CON, 0xC7
mov [0xFB42], ~ZEROS ; *(0xFB42) = 0x0000; TODO: Unknown loc
mov ~T3IC, 0x50
bfldh ~PSW, 0xF8, 0x0 ; PSW: ILVL 15, IEN 1 (highest priority, interrupts enabled)
jmps 0x00, 0xFC1C ; see entryNextStage TODO: replace with label reference OEW


;; memcpy (word-sized) ;;
; r13  src 
; r3   dest
; r14  final dest (exclusive)
.memcpy_loop:
mov r2, [r13+]
mov [r3], r2
add r3, 0x2 ; TODO: Increment in prev. instruction OEW

memcpy:
cmp r3, r14
jmpr cc_neq, .memcpy_loop
ret

;;

@org 0xFC00
;;; Code to be copied to 0xFC00 ;;;
; So this must be 0xFA98 in the received data scope.
; It's only executed from 0xFC00 though.
; Also code is 90 bytes long

;; This is the entry for Timer 3 interrupt, which is activated in stage 1
scxt r1, [0xFB42] ; Push r1; r1 = *(0xFB42) ; TODO: Unknown loc
cmpi1 r1, 0x14 ; (r1++ < 0x14) ? 
jmpr cc_ult, lb_2  
bclr ~T3CON.6 ; if no disable Timer 3 (T3R = 0)

lb_2:
movb [0xFB42], rl1 ; *(0xFB42) = r1 (equal to (*b(0xFB42) + 1) )
bmovn ~P4.1, ~P4.1 ; toggle P4.1
mov ~T3, 0x0C62 ; 3170 ticks (~0.1s again)
pop r1 ; restore r1
reti

;; Stage 0.5 so to say - loads code to call into 0xFA00
; @org 0xFC1C
entryNextStage:
callr waitForRX
movbz r4, [&S0RBUF]
mov r0, 0xFA00 ; r0 = 0xFA00
add r4, r0 ; r4 = received byte + 0xFA00 (delimiter for new code segment)
movb rl5, 0x0 ; rl5 = 0

;; Receives Boot code via serial and moves it to 0xFA00 ..
rxBootCode:
  callr waitForRX
  movb [r0], [&S0RBUF] ; Move byte to code area
  xorb rl5, [r0+] ; Update rolling XOR checksum
  cmp r0, r4
  jmpr cc_neq, rxBootCode ; Loop until delimiter (initial rx + 0xFA00) is received

;; Receive Checksum. If OK proceed, else tx 0x5A and jump back to entry
callr waitForRX
cmpb rl5, [&S0RBUF]
jmpr cc_eq, checksumOK
mov ~S0TBUF, 0x5A 
jmpr cc_uc, entryNextStage 

checksumOK:
calls 0x00, 0xFA00 ; Call received code 

;; Re-Entry after Stage 1
;; Tx 0xA5 
mov ~S0TBUF, 0xA5
bclr S0TIR 

waitForTxCompletion:
  jnb S0TIR, waitForTxCompletion

jmpr cc_uc, entryNextStage

;; Loops until a byte has been rxed
waitForRX:
jnb S0RIR, waitForRX 
bclr S0RIR
ret

@padAbsolute 0x0000FE 0xFF 



