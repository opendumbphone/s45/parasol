;; Remember to send length of segment and checksum before and after code
@org 0x0200
jmpa cc_uc, enter

enter:
  calla cc_uc, initT3andIRQs
  calla cc_uc, extraBusSetup
  calla cc_uc, detectDoubleFlash
  calla cc_uc, setupAfterTrigger
  calla cc_uc, getFlashHC ; into r9

  mov r10, 0x1DC8 ; "Phone ID IN RAM 1"
  mov r0, 0
  calla cc_uc, lb07c8 ; "Get Flash Serial Number and Check HC"

  calla cc_uc, lb07a4 ; "Get_PhoneID_2nd"

  mov r3, 0x4B4F ; "OK"
  calla cc_uc, txWord ; Tx word

mainloop:
  callr rxByte
  cmpb rl3, @ascii('H')
    jmpa cc_eq, cmdH ; -> 0x0C58
  cmpb rl3, @ascii('B')
    jmpa cc_eq, cmdB ; -> 0x1720
  cmpb rl3, @ascii('J')
    jmpa cc_eq, cmdJ ; -> 0x03FA ; freia does not support this
  cmpb rl3, @ascii('S')
    jmpa cc_eq, cmdS ; -> 0x175E
  cmpb rl3, @ascii('R')
    jmpa cc_eq, cmdR ; -> 0x12C6
  cmpb rl3, @ascii('F')
    jmpa cc_eq, cmdF ; -> 0x1844
  cmpb rl3, @ascii('I')
    jmpa cc_eq, cmdI ; -> 0x0446
  cmpb rl3, @ascii('I')
    jmpa cc_eq, cmdI ; -> 0x0446
  cmpb rl3, @ascii('Q')
    jmpa cc_eq, cmdQ ; -> 0x05F2
  cmpb rl3, @ascii('W')
    jmpa cc_eq, cmdW ; -> 0x18D0
  cmpb rl3, @ascii('V')
    jmpa cc_eq, cmdV ; -> 0x0CF0
  cmpb rl3, @ascii('A')
    jmpa cc_eq, cmdA ; -> 0x0CFC
  cmpb rl3, @ascii('C')
    jmpa cc_eq, cmdC ; -> 0x0DF2
  cmpb rl3, @ascii('D')
    jmpr cc_eq, cmdD ; -> 0x0322
  cmpb rl3, @ascii('P')
    jmpr cc_eq, cmdP ; -> 0x0EFC
  jmpr cc_uc, mainloop

;;; SUBROUTINES
;@org 0x02A0
txByte:
  bclr ~S0TIC.7
  movbz [&S0TBUF], rl3
  .waitForTX:
    jnb ~S0TIC.7, .waitForTX
  ret

;@org 0x02AC
rxByte:
  jnb ~S0RIC.7, rxByte
  bclr ~S0RIC.7
  movbz r3, [&S0RBUF]
  ret
  

;@org 0x0616
;; TX rl3, rh3 to Serial0
txWord:
  push r3
  calla cc_uc, txByte
  movb rl3, rh3
  calla cc_uc, txByte
  pop r3
  ret

; @org 0x0634
;; TODO: No Idea why or how this works
getFlashHCId:
  mov ~DPP1, 0x0200
  mov r12, 0x4000
  mov r11, 0x4AAA
  mov r10, 0x4555
  mov r4, 0x55AA
  mov r3, 0x0090

  movb [r11], rl4 ; *b(0x800AAA) = 0xAA
  movb [r10], rh4 ; *b(0x800555) = 0x55
  movb [r11], rl3 ; *b(0x800AAA) = 0x90
  mov r5, [r12 + 0x0] ; r5 = *(0x800000)
  mov r6, [r12 + 0x2] ; r6 = *(0x800002)
  movb rl3, 0xF0
  movb [r11], rl3 ; *b(0x800AAA) = 0xF0
  ret

; @org 0x0662
;; TODO: find out how this routine really works
detectDoubleFlash:
  mov r0, 0xE009
  bclr ~BUSCON4.10 ; Disable Bus4
  mov [&ADDRSEL4], r0 ; Bus4: 2MB at (0xE00000 .. 0xFFFFFF)
  callr getFlashHCId
  
  mov r7, r5 ; results from routine
  mov r8, r6

  mov r0, 0x8009
  bset ~BUSCON4.10 ; Enable Bus4
  mov [&ADDRSEL4], r0 ; Bus4: 2MB at (0x800000 .. 0x9FFFFF)
  callr getFlashHCId

  movb rl3, 2
  extp 0x0, 1
  movb [0x1D8C], rl3 ; *b(0x001D8C) = 2 ; BC35_type

  mov r0, 0xE009
  mov [&ADDRSEL4], r0 ; 2MB at (0xE00000 .. 0xFFFFFF)
  
  cmp r5, r7
  jmpr cc_ne, .lb0698 ; we DO have one chip on board
  cmp r6, r8
  jmpr cc_eq, .lb06a4

  .lb0698:
    bclr ~BUSCON4.10 ; Disable Bus4
    movb rl3, 1
    extp 0x0, 1
    movb [0x1D8C], rl3 ; *b(0x001D8C) = 1 ; BC35_type
  .lb06a4:
    r15, 0x8009
    extp 0x0, 1
    movb rl3, [0x1D8C] ; BC35_type
    cmpb rl3, 2 
    jmpr cc_eq, .lb06b8
    mov r15, 0xE009
  .lb06b8:
    extp 0x0, 1  
    mov [0x1D8C], r15 ; SAVED_ADDRSEL4

  ret

; @org 0x06c2
;; TODO: Purpose of this routine
;; In: r5, r6, r12
;; Out: r6, *(0x001D88)
lb06c2:
  push r4
  mov r4, 0
  cmp r5, 1
  jmpr cc_ne, .lb06d6 ; (r5 != 1) -> *(0x001D88) = 0
  ; else *(0x001D88) = 1
  
  mov r4, 1
  cmp r6, 0x227E
  jmpr cc_ne, .lb06d6
  
  mov r6, [r12 + 0x1C] ; (r6 == 0x227E) -> r6 = *(r12 + 0x1C)

  .lb06d6:
    extp 0x0, 1
    mov [0x1D88], r4
  
  pop r4
  ret

; @org 0x06e2
; TODO: Find out how this works
getFlashHC:
  mov ~DPP1, 0x0200 ; Default 1st flash ID
  mov r12, 0x4000
  mov r11, 0x4AAA
  mov r10, 0x4555
  mov r4, 0x55AA
  mov r3, 0x90FF

  movb [r11], rl4 ; *b(0x800AAA) = 0xAA
  movb [r10], rh4 ; *b(0x800555) = 0x55
  movb [r11], rh3 ; *b(0x800AAA) = 0x90

  mov r0, 0x1D8C ; "Flash_HC_RAM_loc"
  mov r5, [r12 + 0x0] ; r5 = *(0x800000)
  mov r6, [r12 + 0x2] ; r6 = *(0x800002)
  callr lb06c2 ; Note: this call does not exist in freia

  ; *(0x001D8C .. 0x001D8F) = [rl5, rh5, rl6, rh6]
  movb [r0 + 0x0], rl5
  movb [r0 + 0x1], rh5
  movb [r0 + 0x2], rl6
  movb [r0 + 0x3], rh6

  movb [r11], rl3 ; *b(0x800AAA) = 0xFF
  movb rh5, rl6
  mov r9, r5 ; "R9 is used during flashtype detection - although it will be overridden later"

  mov ~DPP1, 0x0300 ; Default 2nd Flash ID
  mov r0, 0x1DC0 ; "Flash_HC_RAM_loc2"
  
  movb [r11], rl4 ; *b(0xC00AAA) = 0xAA
  movb [r10], rh4 ; *b(0xC00555) = 0x55
  movb [r11], rh3 ; *b(0xC00AAA) = 0x90

  mov r5, [r12 + 0x0] ; r5 = *(0xC00000)
  mov r6, [r12 + 0x2] ; r6 = *(0xC00002)
  callr lb06c2 ; Note: this call does not exist in freia

  ; *(0x001DC0 .. 0x001DC3) = [rl5, rh5, rl6, rh6]
  movb [r0 + 0x0], rl5
  movb [r0 + 0x1], rh5
  movb [r0 + 0x2], rl6
  movb [r0 + 0x3], rh6

  movb [r11], rl3 ; *b(0xC00AAA) = 0xFF
  movb rh5, rl6
  ret


; @org 0x0752
initT3andIRQs:
  ;; T3 *= 3
  mov r12, [&T3]
  mov r13, r12
  shl r12, 2
  add r12, r13
  mov [&T3], r12

  ;; Set Interrupt Vector for Timer 3
  ;; *(0x008C .. 0x008F) = (FA 00 B8 02) -> JMPS 0x00, 0x02B8
  mov ~DPP0, 0x0
  mov r1, 0x8C
  mov r2, 0x00FA
  mov r3, 0x02B8
  mov [r1 + 0x0], r2
  mov [r1 + 0x2], r3 

  ;; Start Timer 3: 32.23 kHz Count Down, Interrupt enabled
  mov ~T3IC, 0x10 ; T3IC: T3IR=0 T3IE=0 ILVL=0b1000 GLVL=0
  mov ~T3CON, 0xC7 ; T3CON: T3I=0b111 T3M=000 T3R=1 T3UD=1
  bset T3IC.6 ; T3IE=1
  mov ~T3, 0x0C62 ; 3170 Ticks ~> 0.01s

  bset ~P6.0 ; TODO: Why?
  bset ~DP6.0

  ;; Disable Serial0 Interrupts
  bclr ~S0TIC.6
  bclr ~S0RIC.6

  bset ~T3CON.6 ; Run Timer 3 TODO: should already be running?
  bset ~PSW.11 ; Enable CPU interrupts

  ;; *(0x001D8C) = 0xE009 TODO: Location Purpose (called SAVED_ADDRSEL4)
  mov r15, 0xE009
  extp 0x0, 1 
  mov [0x1D8C], r15

  mov ~DPP0, 0x0000
  ret

;@org 0x07A4
; "Get_PhoneID_2nd"
lb07a4:
  mov ~DPP0, 0x0000
  mov r10, 0x1DC4 ; "HC from flash in RAM"
  extp 0x021F, 2
  mov r9, [0x3E18] ; r9 = *(0x880018)
  mov r0, [0x3E18] ; r0 = *(0x880018)
  movb [r10 + 0x0], rl0 ; *(0x001DC4) = rl0
  movb [r10 + 0x2], rh0 ; *(0x001DC6) = rh0
  mov r10, 0x1DD8 ; "Phone ID 2nd AT RAM"
  mov r0, 1 ; skipReset is active
  jmpr cc_uc, .lb07c8 ; Call ID routine again, presumably for the second flash

;@org 0x07C8
; "Get Flash Serial Number and Check HC"
; IN
;   r0    bool  skipReset?
;   r9    w     HC (possibly HarwareCode? idk. Seems to be IDing the flash somehow. Set previously)
;   r10   *w    offset
lb07c8:
  mov ~DPP0, 0x0200
  mov ~DPP1, 0x0000
  add r10, 0x4000

  ;; TODO: re-order and optimize OEW
  cmp r9, 0x1689
    jmpa cc_eq, .lb0c0e
  cmp r9, 0x1789
    jmpa cc_eq, .lb0c0e
  cmp r9, 0x1720
    jmpa cc_eq, .lb0c0e
  cmp r9, 0x172C
    jmpa cc_eq, .lb0c0e
  
  cmp r9, 0x9289
    jmpa cc_eq, .lb0bbc
  cmp r9, 0x9689
    jmpa cc_eq, .lb0bbc

  cmp r9, 0x5489
    jmpa cc_eq, .lb0c0e

  cmp r9, 0xC089
    jmpa cc_eq, .lb0bbc
  cmp r9, 0xC289
    jmpa cc_eq, .lb0bbc
  cmp r9, 0xC489
    jmpa cc_eq, .lb0bbc
  cmp r9, 0xC589
    jmpa cc_eq, .lb0bbc
  cmp r9, 0xBA20
    jmpa cc_eq, .lb0bbc
  cmp r9, 0xCE20
    jmpa cc_eq, .lb0bbc
  cmp r9, 0x4820
    jmpa cc_eq, .lb0bbc

  cmp r9, 0x1020
    jmpa cc_eq, .lb0c0e

  cmp r9, 0x9020
    jmpa cc_eq, .lb0a8e

  cmp r9, 0xA220
    jmpa cc_eq, .lb0ad8

  cmp r9, 0xF201
    jmpa cc_eq, .lb09f6

  cmp r9, 0xE401
    jmpr cc_eq, .lb0932
  cmp r9, 0xE404
    jmpr cc_eq, .lb0932
  cmp r9, 0xB701
    jmpr cc_eq, .lb0932

  ;; Following entries are not in freia:
  cmp r9, 0x1201
    jmpr cc_eq, .lb08a2
  cmp r9, 0x0C01
    jmpr cc_eq, .lb08a2

  ;; Default case
  ;; If Flash type is unknown, tx error (0xCCCC) and Flash type to Serial0, then exit
  ;; Skipped if r0 (skipReset) is set
  cmp r0, 1
  jmpr cc_eq, .skipReset
    mov r3, 0xCCCC
    calla cc_uc, txWord 
    mov r3, r9
    calla cc_uc, txWord
    jmpa cc_uc, lb05f2 ; "cmd_q"
  .skipReset:
    ret

  ;; HC: 0x1201 0x0C01
  ;; (Not included in freia)
  .lb08a2:
    push ~BUSCON0
    and ~BUSCON0, 0xFFFE ; ~BUSCON0.0 = 0 (lowest bit of MCTC)
    mov r0, 0
    mov r1, 0x90F0
    mov r3, 0x55AA
    movb [0x0AAA], rl3
    movb [0x0555], rh3
    movb [0x0AAA], rh1
    mov r2, [r0+]
    mov r4, [r0]
    movb rh1, 0x0088
    movb [0x0AAA], rl3
    movb [0x0555], rh3
    movb [0x0AAA], rh1
    mov r0, 0
    xor r2, [r0+]
    xor r4, [r0+]
    rol r4, 4
    ror r2, 5
    xor r2, [r0+]
    xor r4, [r0]
    rol r4, 2
    ror r2, 3
    mov r0, 0
    movb [r0], rl1
    mov r0, 0
    mov r1, 0x90F0
    mov r3, 0x55AA
    movb [0x0AAA], rl3
    movb [0x0555], rh3
    movb [0x0AAA], rh1
    movb [0x0000], rl1
    mov r0, r4
    addb rl0, rh0
    xorb rl0, 0x12
    addb rl0, 7
    shl r0, 2
    mov r1, r2
    xorb rl1, rh1
    xorb rl1, 0x16
    subb rl1, 0x12
    xorb rl1, rl0
    movb rh1, rl1
    movb [r10 + 0x0], rl2
    movb [r10 + 0x1], rh2
    movb [r10 + 0x2], rl4
    movb [r10 + 0x3], rh4
    pop ~BUSCON0
    ret

  ;; HC 0xE401 0xE404 0xB701
  ;; "HC_Grp2_GetPhoneID"
  .lb0932:
    push     r14
    mov      r14, r10
    mov      DPP0, 0x0200
    mov      r6, 7
    mov      r5, 0
    cmp      r9, 0xE401
    jmpr     CC_Z_EQ, .lb095c
    mov      r5, 0x0780
    mov      DPP0, 0x02FC
    cmp      r9, 0xB701
    jmpr     CC_Z_EQ, .lb095c
    mov      r6, 3
    mov      r5, 0x3F00
    mov      DPP0, 0x027F
    .lb095c: ; "AMD_Get_PhoneID_Cont"
    mov      r1, 0x0AAA
    mov      r2, 0x0555
    mov      r4, 0x9088
    mov      r8, 0
    movb     [r1], rl1
    movb     [r2], rl2
    movb     [r1], rl4
    .lb0970: ; "AMD_Get_PhoneID_Loop"
    mov      r7, [r5+]
    rol      r7, r6
    movb     [r14+0x0000], rl7
    movb     [r14+0x0001], rh7
    add      r14, 2
    cmpd1    r6, 0
    jmpr     CC_NZ_NE, .lb0970
    movb     [r1], rl1
    movb     [r2], rl2
    movb     [r1], rh4
    mov      r3, 0
    mov      [r5], r3
    movb     [r1], rl1
    movb     [r2], rl2
    movb     [r1], rh4
    push     r9
    mov      r9, [0x0006]
    shr      r9, 2
    mov      r0, 0x0090
    bmovn    r9.5, r0.5
    pop      r9
    movb     rl1, [r10+0x0001]
    movb     rh1, [r10+0x0003]
    add      r1, r0
    mov      r3, 0x00F0
    movb     [r10+0x0003], rl1
    movb     [r10+0x0001], rh1
    movb     [r5], rl3
    mov      r14, r10
    mov      r0, 8
    cmp      r9, 0xE404
    jmpr     CC_NZ_NE, .lb09c8
    mov      r0, 4
    .lb09c8: ; "AMD_PhoneID_NOT_E404"
    mov      r2, 0
    mov      r3, 0
    .lb09cc: ; "AMD_PhoneID_XOR_Loop"
    mov      r5, [r14+]
    xorb     rl2, rl5
    xorb     rh2, rh5
    movb     rl4, rh2
    movb     rh4, rh3
    movb     rh2, rl2
    movb     rh3, rl3
    movb     rl2, rh4
    movb     rl3, rl4
    sub      r0, 1
    jmpr     CC_NZ_NE, .lb09cc
    movb     [r10+0x0000], rl2
    movb     [r10+0x0001], rh2
    movb     [r10+0x0002], rl3
    movb     [r10+0x0003], rh3
    pop      r14
    ret

  ;; HC 0xF201
  ;; "HC_Grp4_GetPhoneID". Rest is completely undocumented
  .lb09f6:
    mov      r1, 0x90F0
    mov      r6, 0x55AA
    mov      r2, 0x0AAA
    mov      r7, 0x0555
    mov      r9, 0
    mov      r5, 0
    mov      r3, [0xFF0C]
    mov      r4, [0xFF0C]
    and      r4, 0xFFF0
    add      r4, 0x000E
    mov      [0xFF0C], r4
    movb     [r2], rl6
    movb     [r7], rh6
    movb     [r2], rh1
    mov      r4, [r9+]
    mov      r0, [r9+]
    add      r4, r0
    mov      r0, 0
    mov      r7, 0
    .lb0a2e:
    mov      r6, [r9]
    cmp      r6, 0
    jmpr     CC_Z_EQ, .lb0a36
    add      r0, 1
    .lb0a36:
    cmp      r7, 0x000F
    jmpr     CC_UGT, .lb0a42
    orb      rl5, rl6
    rol      r5, 1
    jmpr     CC_UC, .lb0a44
    .lb0a42:
    rol      r4, r6
    .lb0a44:
    add      r7, 1
    cmp      r7, 0x0013
    jmpr     CC_Z_EQ, .lb0a76
    cmp      r7, 0x0010
    jmpr     CC_Z_EQ, .lb0a64
    cmp      r7, 0x0011
    jmpr     CC_Z_EQ, .lb0a70
    cmp      r7, 0x0012
    jmpr     CC_Z_EQ, .lb0a6a
    add      DPP0, 0x0004
    jmpr     CC_UC, .lb0a2e
    .lb0a64:
    add      DPP0, 0x0002
    jmpr     CC_UC, .lb0a2e
    .lb0a6a:
    add      DPP0, 0x0001
    jmpr     CC_UC, .lb0a2e
    .lb0a70:
    mov      r9, 0x2004
    jmpr     CC_UC, .lb0a2e
    .lb0a76:
    movb     [r9], rl1
    movb     [r10+0x0000], rl4
    movb     [r10+0x0001], rh4
    movb     [r10+0x0002], rl5
    movb     [r10+0x0003], rh5
    mov      [0xFF0C], r3
    ret

  ;; HC 0x9020
  ;; "HC_9020_GetPhoneID"
  .lb0a8e:
    mov      r0, 0
    mov      r1, 0x90FF
    mov      r3, [0xFF0C]
    mov      r4, [0xFF0C]
    and      r4, 0xFFF0
    add      r4, 0x000E
    mov      [0xFF0C], r4
    movb     [r0], rh1
    mov      r4, [r0+]
    mov      r5, [r0+]
    mov      r0, 0x0102
    .lb0ab2:
    xor      r4, [r0+]
    xor      r5, [r0+]
    cmp      r0, 0x010A
    jmpr     cc_ne, .lb0ab2
    xor      r4, [r0]
    xor      r5, [r0]
    movb     [r0], rl1
    movb     [r10+0x0000], rl4
    movb     [r10+0x0001], rh4
    movb     [r10+0x0002], rl5
    movb     [r10+0x0003], rh5
    mov      [0xFF0C], r3
    ret


  ;; HC 0xA220
  ;; "HC_Grp3_GetPhoneID"
  .lb0ad8:
    mov      r0, 0
    mov      r1, 0x98F0
    mov      r2, 0x0AAA
    mov      r3, [0xFF0C]
    mov      r4, [0xFF0C]
    and      r4, 0xFFF0
    add      r4, 0x000E
    mov      [0xFF0C], r4
    movb     [r2], rh1
    mov      r4, [r0+]
    mov      r5, [r0+]
    add      r0, 0x00FC
    xor      r4, [r0+]
    xor      r5, [r0+]
    movb     [r0], rl1
    movb     [r10+0x0000], rl4
    movb     [r10+0x0001], rh4
    movb     [r10+0x0002], rl5
    movb     [r10+0x0003], rh5
    mov      [0xFF0C], r3
    ret

  ;; HC 0x9289 0x9689 0xC089 0xC289 0xC489 0xC589 0xBA20 0xCE20 0x4820
  ;; "HC_Grp1_GetPhoneID"
  .lb0bbc:
    mov r0, 0
    mov r2, 0
    mov r1, 0x90FF
    mov r3, [0xFF0C]
    mov r4, [0xFF0C]
    and r4, 0xFFF0
    add r4, 0x000E
    mov [0xFF0C], r4
    movb [r2], rh1
    mov r4, [r0+0x0000]
    mov r5, [r0+0x0002]
    mov r0, 0x0102
    xor r4, [r0+]
    xor r5, [r0+]
    rol r5, 1
    ror r4, 2
    xor r4, [r0+]
    xor r5, [r0+]
    ror r4, 3
    rol r5, 4
    movb [r2], rl1
    movb [r10+0x0000], rl4
    movb [r10+0x0001], rh4
    movb [r10+0x0002], rl5
    movb [r10+0x0003], rh5
    mov [0xFF0C], r3
    ret
    

  ;; HC 0x1689 0x1789 0x1720 0x172C 0x5489 0x1020
  ;; "HC_Grp1a_GetPhoneID"
  .lb0c0e:
    mov r0, 0
    mov r2, 0
    mov r1, 0x90FF
    movb [r2 + 0x0], rh1
    mov r4, [r0 + 0x0]
    mov r5, [r0 + 0x2]
    mov r0, 0x0102
    xor r4, [r0+]
    xor r5, [r0+]
    rol r5, 4
    ror r4, 5
    xor r4, [r0+]
    xor r5, [r0+]
    rol r5, 2
    ror r4, 3
    movb [r2], rl1
    movb [r10+0], rl4
    movb [r10+1], rh4
    movb [r10+2], rl5
    movb [r10+3], rh5
    ret
;@org 0x1B9E
;; Bus Setup among other unknown things
extraBusSetup:
  ;; TODO: Unknown registers at 0xF134, 0xF136, 0xF13E
  extr 3
  mov 0x9A, 0x9800
  mov 0x9B, 0x0800
  mov 0x9F, 0x0006

  ;; TODO: Purpose
  mov ~P8, 0xFFFF
  mov ~DP8, 0xFFFF
  mov ~P6, 0x0401
  mov ~DP6, 0x0501

  ;; Bus 1: 2MB starting at 0x000000 (0x000000 .. 0x1FFFFF) ; TODO: Does this make sense?
  ;; Bus 2: 4MB starting at 0xC00000 (0xC00000 .. 0xFFFFFF)
  mov ~ADDRSEL1, 0x0009
  mov ~ADDRSEL2, 0xC00A
  mov ~BUSCON1, 0x05AD ; Bus1: MCTC=0b1101 RWDC1=0 MTTC1=1 BTYP=0b10 .8=1 ALECTL1=0 BUSACT1=1
  mov ~BUSCON2, 0x05AF ; Bus2: MCTC=0b1111 RWDC1=0 MTTC1=1 BTYP=0b10 .8=1 ALECTL1=0 BUSACT1=1
  ret

;; TODO: completely useless, replace OEW
setupAfterTrigger:
  ret

;; Data segment starts here
; TODO: Data Segment

