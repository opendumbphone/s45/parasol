@regset s45
@define S0TIR ~S0TIC.7
@define S0RIR ~S0RIC.7

; TX 0xA6
movb rl3, 0xA6
callr txByte

; Receive two bytes into rl2, rh2
callr rxByte
movb rl2, rl3
callr rxByte
movb rh2, rl3

mov r4, 0x0200 ; Code destination
movb rl5, 0x0 ; Rolling XOR initial value
mov ~DPP0, 0x0

;; Receive r2 bytes from Serial0 and write them to *(0x0200 ..)
rxCodeSegment:
  callr rxByte
  movb [r4], rl3
  xorb rl5, rl3
  add r4, 0x1
  sub r2, 0x1
  jmpr cc_nz, rxCodeSegment

;; Receive Checksum, reset system if Rolling XOR is invalid
callr rxByte
cmpb rl3, rl5
jmpr cc_ne, chksumInvalid

;; Jump to received code
jmps 0x00, 0x0200

;; TX 0x5A and reset machine
chksumInvalid:
  movb rl3, 0x5A
  callr txByte
  srst

;; TX rl3 to Serial0
txByte:
  movbz r3, rl3
  mov [&S0TBUF], r3
  bclr S0TIR
  .waitForTX:
    jnb S0TIR, .waitForTX
  ret

;; RX byte from Serial0 into r3
rxByte:
  .waitForRX:
    jnb S0RIR, .waitForRX
  bclr S0RIR
  movbz r3, [&S0RBUF]
  ret
  
