# Parasol
Bootstrap loader (bsl) for S45 and ME45, providing functionality for flashing and/or dumping firmware. Based on the age-old freia project, aiming to provide a more up-to-date and well-documented approach.

\[ THE PROJECT IS NOT IN WORKING ORDER YET. PLEASE DO NOT USE ANY OF THE SCRIPTS PROVIDED.\]

This software comes with absolutely no warranty. The developers cannot be held responsible if you manage to fry your phone with this software.

## Getting started
### Preparing the hardware
Make sure that you have a working patch cable! The official Siemens RS-232 cable does not work, as it draws 4.5V from the phone in order to operate. However, injecting bsl code needs to take place prior to the phone providing the voltage. You need to modify the phone connector part of the official cable so that you remove the white wire from the phone connector and inject +4.5V into it, using the exposed metal of the black cable on the connector as a common ground. Refer to the pinouts listed in the service manual for details, if necessary.

### Using the BSL as a shell (precompiled package)
TODO: This should be the preferred way to use Parasol and needs to be implemented

### Compiling the loader yourself
Run `acigg.py -p parasol.yml`. The binaries should now be in `out/`. Refer to the protocol description for further info on how to use these.

## Roadmap
Actually write the thing.

## Contributing
Feel free to submit issues and pull requests. However, the bsl assembly code will probably stay as close to freia as possible for safety reasons, so do not expect huge changes there.

## Authors and acknowledgment
Based on Griffin's [freia](https://github.com/siemens-mobile-dev/freia/)

## License
tbd
